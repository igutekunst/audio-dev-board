EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Micro_SD_Card_Det J8
U 1 1 61EEEB73
P 5950 3300
F 0 "J8" H 5900 4117 50  0000 C CNN
F 1 "Micro_SD_Card_Det" H 5900 4026 50  0000 C CNN
F 2 "Connector_Card:microSD_HC_Hirose_DM3D-SF" H 8000 4000 50  0001 C CNN
F 3 "https://www.hirose.com/product/en/download_file/key_name/DM3/category/Catalog/doc_file_id/49662/?file_category_id=4&item_id=195&is_series=1" H 5950 3400 50  0001 C CNN
	1    5950 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2900 5050 2900
Wire Wire Line
	3750 3000 5050 3000
Wire Wire Line
	4350 3100 5050 3100
Wire Wire Line
	3750 3500 5050 3500
Wire Wire Line
	3750 3600 5050 3600
Text Notes 5200 4200 0    50   ~ 0
TODO: Consider putting SD Card in own sheet to clean up data lines
Wire Wire Line
	4350 3300 5050 3300
Wire Bus Line
	3650 2700 3550 2600
Wire Bus Line
	3550 2600 3150 2600
Entry Wire Line
	3650 2800 3750 2900
Entry Wire Line
	3650 2900 3750 3000
Entry Wire Line
	3650 3500 3750 3600
Entry Wire Line
	3650 3400 3750 3500
Text HLabel 4350 3300 0    50   Input ~ 0
SD_CLK
Text HLabel 4350 3100 0    50   Input ~ 0
SD_CMD
Wire Wire Line
	5050 3800 3150 3800
Wire Wire Line
	5050 3200 4750 3200
Wire Wire Line
	4750 3200 4750 2700
Wire Wire Line
	5050 3400 4750 3400
Wire Wire Line
	4750 3400 4750 3900
$Comp
L power:+3.3V #PWR0123
U 1 1 61EF0EF6
P 4750 2700
F 0 "#PWR0123" H 4750 2550 50  0001 C CNN
F 1 "+3.3V" H 4765 2873 50  0000 C CNN
F 2 "" H 4750 2700 50  0001 C CNN
F 3 "" H 4750 2700 50  0001 C CNN
	1    4750 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0124
U 1 1 61EF15CF
P 4750 3900
F 0 "#PWR0124" H 4750 3650 50  0001 C CNN
F 1 "GND" H 4755 3727 50  0000 C CNN
F 2 "" H 4750 3900 50  0001 C CNN
F 3 "" H 4750 3900 50  0001 C CNN
	1    4750 3900
	1    0    0    -1  
$EndComp
Text HLabel 3150 2600 0    50   BiDi ~ 0
SDMMC_D[3..0]
$Comp
L power:+3.3V #PWR0121
U 1 1 62AC4555
P 3150 3400
F 0 "#PWR0121" H 3150 3250 50  0001 C CNN
F 1 "+3.3V" H 3165 3573 50  0000 C CNN
F 2 "" H 3150 3400 50  0001 C CNN
F 3 "" H 3150 3400 50  0001 C CNN
	1    3150 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 62AC5598
P 3150 3850
F 0 "#PWR0122" H 3150 3600 50  0001 C CNN
F 1 "GND" H 3155 3677 50  0000 C CNN
F 2 "" H 3150 3850 50  0001 C CNN
F 3 "" H 3150 3850 50  0001 C CNN
	1    3150 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3800 3150 3850
$Comp
L Device:R_Small_US R58
U 1 1 62AC73EC
P 3150 3550
F 0 "R58" H 3218 3596 50  0000 L CNN
F 1 "10K" H 3218 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3150 3550 50  0001 C CNN
F 3 "~" H 3150 3550 50  0001 C CNN
	1    3150 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3400 3150 3450
Wire Wire Line
	3150 3650 3150 3700
Wire Wire Line
	3150 3700 5050 3700
Wire Wire Line
	3150 3700 2750 3700
Connection ~ 3150 3700
Text HLabel 2750 3700 0    50   Output ~ 0
~SD_DETECT
Text Label 4000 2900 0    50   ~ 0
SDMMC_D2
Text Label 4000 3000 0    50   ~ 0
SDMMC_D3
Text Label 4000 3500 0    50   ~ 0
SDMMC_D0
Text Label 4000 3600 0    50   ~ 0
SDMMC_D1
$Comp
L power:GND #PWR0164
U 1 1 619CD1B1
P 6850 3850
F 0 "#PWR0164" H 6850 3600 50  0001 C CNN
F 1 "GND" H 6855 3677 50  0000 C CNN
F 2 "" H 6850 3850 50  0001 C CNN
F 3 "" H 6850 3850 50  0001 C CNN
	1    6850 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3800 6850 3800
Wire Wire Line
	6850 3800 6850 3850
Wire Bus Line
	3650 2700 3650 3500
$EndSCHEMATC
