EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Memory_RAM:W9812G6KH-5 U4
U 1 1 62154DFF
P 4950 3150
F 0 "U4" H 5000 5200 50  0000 C CNN
F 1 "W9812G6KH-5" H 5000 5350 50  0000 C CNN
F 2 "Package_SO:TSOP-II-54_22.2x10.16mm_P0.8mm" H 5050 2850 50  0001 C CNN
F 3 "https://www.winbond.com/resource-files/da00-w9812g6khc1.pdf" H 4550 4400 50  0001 C CNN
	1    4950 3150
	1    0    0    -1  
$EndComp
Wire Bus Line
	6000 3650 6100 3750
Wire Wire Line
	5550 2050 5900 2050
Wire Wire Line
	5550 2150 5900 2150
Wire Wire Line
	5550 2250 5900 2250
Wire Wire Line
	5550 2350 5900 2350
Wire Wire Line
	5550 2450 5900 2450
Wire Wire Line
	5550 2550 5900 2550
Wire Wire Line
	5550 2650 5900 2650
Wire Wire Line
	5550 2750 5900 2750
Wire Wire Line
	5550 2850 5900 2850
Wire Wire Line
	5550 2950 5900 2950
Wire Wire Line
	5550 3050 5900 3050
Wire Wire Line
	5550 3150 5900 3150
Wire Wire Line
	5550 3250 5900 3250
Wire Wire Line
	5550 3350 5900 3350
Wire Wire Line
	5550 3450 5900 3450
Wire Wire Line
	5550 3550 5900 3550
Entry Wire Line
	5900 2050 6000 2150
Entry Wire Line
	5900 2150 6000 2250
Entry Wire Line
	5900 2250 6000 2350
Entry Wire Line
	5900 2350 6000 2450
Entry Wire Line
	5900 2450 6000 2550
Entry Wire Line
	5900 2550 6000 2650
Entry Wire Line
	5900 2650 6000 2750
Entry Wire Line
	5900 2750 6000 2850
Entry Wire Line
	5900 2850 6000 2950
Entry Wire Line
	5900 2950 6000 3050
Entry Wire Line
	5900 3050 6000 3150
Entry Wire Line
	5900 3150 6000 3250
Entry Wire Line
	5900 3250 6000 3350
Entry Wire Line
	5900 3350 6000 3450
Entry Wire Line
	5900 3450 6000 3550
Entry Wire Line
	5900 3550 6000 3650
Text Label 5600 2050 0    50   ~ 0
D0
Text Label 5600 2150 0    50   ~ 0
D1
Text Label 5600 2250 0    50   ~ 0
D2
Text Label 5600 2350 0    50   ~ 0
D3
Text Label 5600 2450 0    50   ~ 0
D4
Text Label 5600 2550 0    50   ~ 0
D5
Text Label 5600 2650 0    50   ~ 0
D6
Text Label 5600 2750 0    50   ~ 0
D7
Text Label 5600 2850 0    50   ~ 0
D8
Text Label 5600 2950 0    50   ~ 0
D9
Text Label 5600 3050 0    50   ~ 0
D10
Text Label 5600 3150 0    50   ~ 0
D11
Text Label 5600 3250 0    50   ~ 0
D12
Text Label 5600 3350 0    50   ~ 0
D13
Text Label 5600 3450 0    50   ~ 0
D14
Text Label 5600 3550 0    50   ~ 0
D15
Wire Bus Line
	6100 3750 6800 3750
Text Label 6400 3750 0    50   ~ 0
D[31..0]
Text HLabel 6800 3750 2    50   BiDi ~ 0
D[31..0]
Wire Wire Line
	4450 3550 4000 3550
Wire Wire Line
	4450 3650 4000 3650
Wire Wire Line
	4450 3750 4000 3750
Wire Wire Line
	4450 4050 4000 4050
Wire Wire Line
	4450 4150 4000 4150
Wire Wire Line
	4450 4250 4000 4250
Text Label 4150 3550 0    50   ~ 0
~CS
Text Label 4150 3650 0    50   ~ 0
CKE
Text Label 4150 3750 0    50   ~ 0
CLK
Text Label 4150 3850 0    50   ~ 0
LDQM
Text Label 4150 3950 0    50   ~ 0
UDQM
Text Label 4150 4050 0    50   ~ 0
~WE
Text Label 4150 4150 0    50   ~ 0
~CAS
Text Label 4150 4250 0    50   ~ 0
~RAS
Text HLabel 4000 3550 0    50   Input ~ 0
~CS
Text HLabel 4000 3650 0    50   Input ~ 0
CKE
Text HLabel 4000 3750 0    50   Input ~ 0
CLK
Text HLabel 4000 4050 0    50   Input ~ 0
~WE
Text HLabel 4000 4150 0    50   Input ~ 0
~CAS
Text HLabel 4000 4250 0    50   Input ~ 0
~RAS
Wire Wire Line
	3500 3850 4450 3850
Wire Wire Line
	3500 3950 4450 3950
Wire Wire Line
	3500 3850 3500 3950
Wire Wire Line
	3500 3950 3500 4050
Connection ~ 3500 3950
$Comp
L power:GND #PWR037
U 1 1 6235E76A
P 3500 4050
F 0 "#PWR037" H 3500 3800 50  0001 C CNN
F 1 "GND" H 3505 3877 50  0000 C CNN
F 2 "" H 3500 4050 50  0001 C CNN
F 3 "" H 3500 4050 50  0001 C CNN
	1    3500 4050
	1    0    0    -1  
$EndComp
Wire Bus Line
	2450 2050 3250 2050
Wire Bus Line
	3350 2150 3250 2050
Entry Wire Line
	3350 3250 3450 3350
Entry Wire Line
	3350 3150 3450 3250
Entry Wire Line
	3350 3050 3450 3150
Entry Wire Line
	3350 2950 3450 3050
Entry Wire Line
	3350 2850 3450 2950
Entry Wire Line
	3350 2750 3450 2850
Entry Wire Line
	3350 2650 3450 2750
Entry Wire Line
	3350 2550 3450 2650
Entry Wire Line
	3350 2450 3450 2550
Entry Wire Line
	3350 2350 3450 2450
Entry Wire Line
	3350 2250 3450 2350
Entry Wire Line
	3350 2150 3450 2250
Wire Wire Line
	3450 2250 4450 2250
Wire Wire Line
	3450 2350 4450 2350
Wire Wire Line
	3450 2450 4450 2450
Wire Wire Line
	3450 2550 4450 2550
Wire Wire Line
	3450 2650 4450 2650
Wire Wire Line
	3450 2750 4450 2750
Wire Wire Line
	3450 2850 4450 2850
Wire Wire Line
	3450 2950 4450 2950
Wire Wire Line
	3450 3050 4450 3050
Wire Wire Line
	3450 3150 4450 3150
Wire Wire Line
	3450 3250 4450 3250
Wire Wire Line
	3450 3350 4450 3350
Text HLabel 3850 2050 0    50   Input ~ 0
BS0
Text HLabel 3850 2150 0    50   Input ~ 0
BS1
Wire Wire Line
	3850 2050 4450 2050
Wire Wire Line
	3850 2150 4450 2150
Text Label 4150 2250 0    50   ~ 0
A0
Text Label 4150 2350 0    50   ~ 0
A1
Text Label 4150 2450 0    50   ~ 0
A2
Text Label 4150 2550 0    50   ~ 0
A3
Text Label 4150 2650 0    50   ~ 0
A4
Text Label 4150 2750 0    50   ~ 0
A5
Text Label 4150 2850 0    50   ~ 0
A6
Text Label 4150 2950 0    50   ~ 0
A7
Text Label 4150 3050 0    50   ~ 0
A8
Text Label 4150 3150 0    50   ~ 0
A9
Text Label 4150 3250 0    50   ~ 0
A10
Text Label 4150 3350 0    50   ~ 0
A11
Text Label 2800 2050 0    50   ~ 0
A[12..0]
Text HLabel 2450 2050 0    50   Input ~ 0
A[12..0]
Wire Wire Line
	4950 1850 4950 1750
Wire Wire Line
	5050 1850 5050 1750
$Comp
L power:+3.3V #PWR038
U 1 1 619571FF
P 5000 1600
F 0 "#PWR038" H 5000 1450 50  0001 C CNN
F 1 "+3.3V" H 5015 1773 50  0000 C CNN
F 2 "" H 5000 1600 50  0001 C CNN
F 3 "" H 5000 1600 50  0001 C CNN
	1    5000 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 1750 5000 1750
Wire Wire Line
	5000 1750 5000 1600
Connection ~ 5000 1750
Wire Wire Line
	5000 1750 5050 1750
$Comp
L Device:C_Small C11
U 1 1 6195D7EF
P 5500 1400
F 0 "C11" H 5592 1446 50  0000 L CNN
F 1 "0.1uF" H 5592 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5500 1400 50  0001 C CNN
F 3 "~" H 5500 1400 50  0001 C CNN
	1    5500 1400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C12
U 1 1 6195E7C7
P 5900 1400
F 0 "C12" H 5992 1446 50  0000 L CNN
F 1 "0.1uF" H 5992 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5900 1400 50  0001 C CNN
F 3 "~" H 5900 1400 50  0001 C CNN
	1    5900 1400
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR039
U 1 1 6195EDB0
P 5700 1150
F 0 "#PWR039" H 5700 1000 50  0001 C CNN
F 1 "+3.3V" H 5715 1323 50  0000 C CNN
F 2 "" H 5700 1150 50  0001 C CNN
F 3 "" H 5700 1150 50  0001 C CNN
	1    5700 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 1250 5700 1250
Wire Wire Line
	5900 1250 5900 1300
Wire Wire Line
	5500 1250 5500 1300
$Comp
L power:GND #PWR040
U 1 1 61964715
P 5700 1650
F 0 "#PWR040" H 5700 1400 50  0001 C CNN
F 1 "GND" H 5705 1477 50  0000 C CNN
F 2 "" H 5700 1650 50  0001 C CNN
F 3 "" H 5700 1650 50  0001 C CNN
	1    5700 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 1150 5700 1250
Connection ~ 5700 1250
Wire Wire Line
	5700 1250 5900 1250
Wire Wire Line
	5500 1500 5500 1600
Wire Wire Line
	5500 1600 5700 1600
Wire Wire Line
	5900 1600 5900 1500
Wire Wire Line
	5700 1600 5700 1650
Connection ~ 5700 1600
Wire Wire Line
	5700 1600 5900 1600
$Comp
L power:GND #PWR0163
U 1 1 619878A7
P 5000 4600
F 0 "#PWR0163" H 5000 4350 50  0001 C CNN
F 1 "GND" H 5005 4427 50  0000 C CNN
F 2 "" H 5000 4600 50  0001 C CNN
F 3 "" H 5000 4600 50  0001 C CNN
	1    5000 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 4450 4950 4500
Wire Wire Line
	4950 4500 5000 4500
Wire Wire Line
	5050 4500 5050 4450
Wire Wire Line
	5000 4500 5000 4600
Connection ~ 5000 4500
Wire Wire Line
	5000 4500 5050 4500
NoConn ~ 6550 5450
NoConn ~ 6550 5350
NoConn ~ 6550 5250
NoConn ~ 6550 5150
NoConn ~ 6550 5050
NoConn ~ 6550 4950
NoConn ~ 6550 4850
NoConn ~ 6550 4750
NoConn ~ 6550 4650
NoConn ~ 6550 4550
NoConn ~ 6550 4450
NoConn ~ 6550 4350
NoConn ~ 6550 4250
NoConn ~ 6550 4150
NoConn ~ 6550 4050
NoConn ~ 6550 3950
Text Label 6150 5450 0    50   ~ 0
D16
Text Label 6150 5350 0    50   ~ 0
D17
Text Label 6150 5250 0    50   ~ 0
D18
Text Label 6150 5150 0    50   ~ 0
D19
Text Label 6150 5050 0    50   ~ 0
D20
Text Label 6150 4950 0    50   ~ 0
D21
Text Label 6150 4850 0    50   ~ 0
D22
Wire Wire Line
	6100 5450 6550 5450
Wire Wire Line
	6100 5350 6550 5350
Wire Wire Line
	6100 5250 6550 5250
Wire Wire Line
	6100 5150 6550 5150
Wire Wire Line
	6100 5050 6550 5050
Wire Wire Line
	6100 4950 6550 4950
Wire Wire Line
	6100 4850 6550 4850
Entry Wire Line
	6000 5350 6100 5450
Entry Wire Line
	6000 5250 6100 5350
Entry Wire Line
	6000 5150 6100 5250
Entry Wire Line
	6000 5050 6100 5150
Entry Wire Line
	6000 4950 6100 5050
Entry Wire Line
	6000 4850 6100 4950
Entry Wire Line
	6000 4750 6100 4850
Text Label 6150 4750 0    50   ~ 0
D23
Text Label 6150 4650 0    50   ~ 0
D24
Text Label 6150 4550 0    50   ~ 0
D25
Text Label 6150 4450 0    50   ~ 0
D26
Text Label 6150 4350 0    50   ~ 0
D27
Text Label 6150 4250 0    50   ~ 0
D28
Text Label 6150 4150 0    50   ~ 0
D29
Text Label 6150 4050 0    50   ~ 0
D30
Text Label 6150 3950 0    50   ~ 0
D31
Wire Wire Line
	6100 4750 6550 4750
Wire Wire Line
	6100 4650 6550 4650
Wire Wire Line
	6100 4550 6550 4550
Wire Wire Line
	6100 4450 6550 4450
Wire Wire Line
	6100 4350 6550 4350
Wire Wire Line
	6100 4250 6550 4250
Wire Wire Line
	6100 4150 6550 4150
Wire Wire Line
	6100 4050 6550 4050
Wire Wire Line
	6100 3950 6550 3950
Entry Wire Line
	6000 4650 6100 4750
Entry Wire Line
	6000 4550 6100 4650
Entry Wire Line
	6000 4450 6100 4550
Entry Wire Line
	6000 4350 6100 4450
Entry Wire Line
	6000 4250 6100 4350
Entry Wire Line
	6000 4150 6100 4250
Entry Wire Line
	6000 4050 6100 4150
Entry Wire Line
	6000 3950 6100 4050
Entry Wire Line
	6000 3850 6100 3950
Connection ~ 6100 3750
Wire Bus Line
	6100 3750 6000 3850
Entry Wire Line
	3350 3250 3450 3350
Wire Wire Line
	3250 3350 2900 3350
Text Label 2950 3350 0    50   ~ 0
A12
NoConn ~ 2900 3350
Entry Wire Line
	3250 3350 3350 3250
Wire Bus Line
	6000 2150 6000 3650
Wire Bus Line
	3350 2150 3350 3250
Wire Bus Line
	6000 3850 6000 5350
$EndSCHEMATC
