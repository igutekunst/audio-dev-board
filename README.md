# Multi Purpose Audio Dev Board

This readme is just a copy of my personall messy notes. Please edit!

This was originally intended to be some kind of USB or Ethernet audio output, and a portable music player.



PLL1700

http://www.ti.com/lit/ds/sbos096a/sbos096a.pdf

http://www.ti.com/lit/ds/slos070d/slos070d.pdf

- [x] Select DAC
- [x] Select MCU
- [ ] Wire up PLL1708
- [ ] Re-wire STM32F756
  - [x] Wire up STM32F7 Power
  - [x] Wire up STM32F7 Clock
  - [x] Wire UP STM32F7 Debug and Trace
  - [x] Wire up STM32F7 SAI
  - [x] Wire up Debug UART 
- [ ] Wire up Headphone AMP
- [ ]  
- [ ] +12V/-12 V Supply for headphone amp
- [ ] Reserve pins  for STM32H7 TFT
- [ ] Consider USB vs Ethernet Clock
- [ ] Wire up Test LEDS and Test Points
- [ ] Make XLR output (and maybe remove the Headphone amp :) )
- [ ] Connecto SD Card
- [ ] Connect USB Connector
- [ ] Connect MIDI connectors
- [ ] Add SPI Test point
- [ ] Add I2S Test Point

possible LCDs:
https://www.mouser.com/ProductDetail/Mikroe/MIKROE-4281?qs=hWgE7mdIu5RSzM1cQSSqfw%3D%3D

Next:
  Add audio output connector
  Add test points/headers for:
    SPI
    I2S
Maybe:

## Clocking Notes
STM32H743
- 3 PLLs
- One external oscillator


Ethernet
USB
SAI


# Parts
[DRV135]


## Future Crazy Ideas
 - Synchronize using PTP
https://sourceforge.net/projects/ptpd/


[DRV135]:https://www.ti.com/product/DRV135


Cheaping out on headphone stage, just use headphone amp: https://www.ti.com/lit/ds/symlink/tpa6133a2.pdf

## Power Supply Considerations
Notes from here: https://www.ti.com/lit/an/sboa237/sboa237.pdf

Article from Analog Devices
https://www.analog.com/en/technical-articles/low-noise-power-supplies-come-in-many-flavors-part-1-linear-regulators.html

Target < 3 uV RMS noise 20Hz-20KHz. The TPS7A4701 can achive this maybe.

To be better, you can use an Op-amp like OPA1688 to achieve < 1uV RMS from 20Hz-20KHz

It seems likely I'll at least need +/- 5V for the current to voltage amplifiers.

The differential to single ended converters also need some power.

What are the noise budgets for various parts of the signal chain?
What is the power requirement for the various parts?

Can an op-amp supply sufficient current? The idea here is for an exteremely low noise voltage rail

DAC
Current to voltage converter
Differential to single ended

Line level differential output

### Rail splitting:
https://github.com/NiHaoMike/OpenDAC-HD/blob/master/OpenDAC_sch_dig.png

Another option: LM27762 generates +/- 5V, with 22uV noise.

Use to LT8330, one for positive, one for negative rail

To get down to 2uV noise:


LT3093
200mA
–1.8V to –20V
0.8 uV Noise 10 Hz to 100KHz
LT3093: https://www.analog.com/media/en/technical-documentation/data-sheets/lt3093.pdf


LT3045
0.8 uV Noise 10 Hz to 100KHz
0-15V
500mA

